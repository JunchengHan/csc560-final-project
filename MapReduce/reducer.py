#!/usr/bin/env python
# coding: utf-8
from operator import itemgetter
import sys

current_youtube_id = None
current_count = 0
youtube_id = None

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    # parse the input we got from mapper.py
    youtube_id, count = line.split('\t', 1)

    # convert count (currently a string) to int
    try:
        count = int(count)
    except ValueError:
        # count was not a number, so silently ignore/discard this line
        continue

    # this IF-switch only works because Hadoop sorts map output
    # by key (here: word) before it is passed to the reducer
    if current_youtube_id == youtube_id:
        current_count += count
    else:
        if current_youtube_id:
            # write result to STDOUT
            print '%s\t%s' % (current_youtube_id, current_count)
        current_count = count
        current_youtube_id = youtube_id

# do not forget to output the last word if needed!
if current_youtube_id == youtube_id:
    print '%s\t%s' % (current_youtube_id, current_count)

