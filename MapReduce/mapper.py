#!/usr/bin/env python
# coding: utf-8
import sys
import string
import re

youtube_rex = r"\"http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?\""
wiki_rex = r"\"http(?:s?):\/\/en\.wikipedia\.org\/wiki\/(.*?)\""
github_rex = r"\"https:\/\/github\.com\/(.*?)\/(.*?)\""

def find_wiki_id(line):
    wiki_ids = re.findall(wiki_rex, line)
    return wiki_ids

def find_youtube_id(line):
    youtube_ids = re.findall(youtube_rex, line)
    return youtube_ids

def find_github_id(line):
    github_ids = re.findall(github_rex, line)
    return github_ids

for line in sys.stdin:
    github_ids = find_github_id(line)
    
    # increase counters
    if len(github_ids) == 1:
        github_id = github_ids[0]
        if len(github_id) == 2:
            # 0: user id 1: repo name
            if len(github_id[0]) > 0 and len(github_id[1]) > 0 and '/' not in github_id[1] and '/' not in github_id[0] and 'href' not in github_id[1] and 'ref' not in github_id[1]:
                print '%s/%s\t%s' % (github_id[0], github_id[1], 1)


# input comes from STDIN (standard input)
# for line in sys.stdin:
#     youtube_ids = find_youtube_id(line)
#     # print(youtube_ids)
#     # increase counters
#     if len(youtube_ids):
#         id_tuple = youtube_ids[0]
#         if len(id_tuple) > 1 and len(id_tuple[0]) > 0:
#             print '%s\t%s' % (id_tuple[0], 1)

# for line in sys.stdin:
#     wiki_ids = find_wiki_id(line)
#     # print(wiki_ids)
#     # increase counters
#     if len(wiki_ids) > 0:
#         wiki_id = wiki_ids[0]
#         if len(wiki_id) > 0:
#             print '%s\t%s' % (wiki_id, 1)

