
## for github data clean

#bad_words = ['ref=', 'rel=', '</a>']
#
#with open('github_data.txt') as oldfile, open('github_data_cleaned.txt', 'w') as newfile:
#	for line in oldfile:
#		if not any(bad_word in line for bad_word in bad_words):
#			line = line.replace("/", "\t")
#			newfile.write(line)
#			
#print('Finished')


with open('wiki_data.txt') as oldfile, open('wiki_data_cleaned.txt', 'w') as newfile:
	from urllib.parse import unquote
	for line in oldfile:
		line = unquote(line)
		newfile.write(line)
			
print('Finished')